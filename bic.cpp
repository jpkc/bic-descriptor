#include <vector>

#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgcodecs.hpp>

void plot_histogram(cv::Mat &img, const cv::Mat &hist, const cv::Scalar cor);
int ColorDistanceSquared(const cv::Mat &img, int x0, int y0, int x1, int y1);
bool Border(const cv::Mat &img, int x, int y, int color_distance_squared_threshold);
cv::Mat BICMask(const cv::Mat &img, int color_distance_squared_threshold);

int main(int argc, char *argv[]) {
	cv::Mat imagem;
	cv::Mat bic_mask, histograma_imagem_bic_border(480, 768, CV_8UC3, cv::Scalar(0, 0, 0)), histograma_imagem_bic_interior(480, 768, CV_8UC3, cv::Scalar(0, 0, 0));
	cv::Mat histograma_imagem(480, 768, CV_8UC3, cv::Scalar(0, 0, 0));
	cv::Mat histograma_tmp_r, histograma_tmp_g, histograma_tmp_b;
	int histograma_bins = 256;

	for(int i = 1; i < argc; i++) {
		cv::namedWindow("imagem", cv::WINDOW_GUI_NORMAL);
		cv::namedWindow("border mask", cv::WINDOW_GUI_NORMAL);
		cv::namedWindow("histograma", cv::WINDOW_AUTOSIZE);
		cv::namedWindow("histograma borda", cv::WINDOW_AUTOSIZE);
		cv::namedWindow("histograma interior", cv::WINDOW_AUTOSIZE);
		imagem = cv::imread(argv[i]);
		if(!imagem.empty()) {
//			Separate the image in 3 places ( B, G and R )
			std::vector<cv::Mat> bgr_planes;
			split(imagem, bgr_planes);

//			Set the ranges ( for B,G,R) )
			float range[] = {0, 256};
			const float* histRange = {range};
			bool uniform = true;
			bool accumulate = false;

//			Compute color histograms:
			cv::calcHist(&bgr_planes[0], 1, 0, cv::Mat(), histograma_tmp_b, 1, &histograma_bins, &histRange, uniform, accumulate);
			cv::calcHist(&bgr_planes[1], 1, 0, cv::Mat(), histograma_tmp_g, 1, &histograma_bins, &histRange, uniform, accumulate);
			cv::calcHist(&bgr_planes[2], 1, 0, cv::Mat(), histograma_tmp_r, 1, &histograma_bins, &histRange, uniform, accumulate);

//			Create histogram image:
			histograma_imagem = cv::Scalar(0, 0, 0);
			plot_histogram(histograma_imagem, histograma_tmp_r, cv::Scalar(0, 0, 255));
			plot_histogram(histograma_imagem, histograma_tmp_g, cv::Scalar(0, 255, 0));
			plot_histogram(histograma_imagem, histograma_tmp_b, cv::Scalar(255, 0, 0));

//			bic calc:
			bic_mask = BICMask(imagem, 98);

//			Compute border histograms:
			cv::calcHist(&bgr_planes[0], 1, 0, bic_mask, histograma_tmp_b, 1, &histograma_bins, &histRange, uniform, accumulate);
			cv::calcHist(&bgr_planes[1], 1, 0, bic_mask, histograma_tmp_g, 1, &histograma_bins, &histRange, uniform, accumulate);
			cv::calcHist(&bgr_planes[2], 1, 0, bic_mask, histograma_tmp_r, 1, &histograma_bins, &histRange, uniform, accumulate);

//			Create histogram image:
			histograma_imagem_bic_border = cv::Scalar(0, 0, 0);
			plot_histogram(histograma_imagem_bic_border, histograma_tmp_r, cv::Scalar(0, 0, 255));
			plot_histogram(histograma_imagem_bic_border, histograma_tmp_g, cv::Scalar(0, 255, 0));
			plot_histogram(histograma_imagem_bic_border, histograma_tmp_b, cv::Scalar(255, 0, 0));

//			Compute interior histograms:
			bic_mask = 255 - bic_mask;
			cv::calcHist(&bgr_planes[0], 1, 0, bic_mask, histograma_tmp_b, 1, &histograma_bins, &histRange, uniform, accumulate);
			cv::calcHist(&bgr_planes[1], 1, 0, bic_mask, histograma_tmp_g, 1, &histograma_bins, &histRange, uniform, accumulate);
			cv::calcHist(&bgr_planes[2], 1, 0, bic_mask, histograma_tmp_r, 1, &histograma_bins, &histRange, uniform, accumulate);

//			Create histogram image:
			histograma_imagem_bic_interior = cv::Scalar(0, 0, 0);
			plot_histogram(histograma_imagem_bic_interior, histograma_tmp_r, cv::Scalar(0, 0, 255));
			plot_histogram(histograma_imagem_bic_interior, histograma_tmp_g, cv::Scalar(0, 255, 0));
			plot_histogram(histograma_imagem_bic_interior, histograma_tmp_b, cv::Scalar(255, 0, 0));

//			Draw windows:
			cv::imshow("imagem", imagem);
			cv::imshow("histograma", histograma_imagem);
			cv::imshow("histograma borda", histograma_imagem_bic_border);
			cv::imshow("histograma interior", histograma_imagem_bic_interior);
			cv::imshow("border mask", bic_mask);
			cv::waitKey(0);
		}
	}
	return 0;
}

void plot_histogram(cv::Mat &img, const cv::Mat &hist, const cv::Scalar cor) {
	if(img.empty() || hist.empty()) {
		return;
	}
	double bin_w = (double)img.cols/(double)hist.rows;
	cv::Mat tmp_hist;
	hist.copyTo(tmp_hist);

//	Normalize the result to [0, histImage.rows]
	cv::normalize(tmp_hist, tmp_hist, img.rows, 0, cv::NORM_MINMAX, -1, cv::Mat());

//	Draw
	cv::Point p0, p1;
	p1 = cv::Point(bin_w*(0), img.rows - cvRound(tmp_hist.at<float>(0)));
	for( int i = 1; i < hist.rows; i++) {
		p0 = p1;
		p1 = cv::Point(bin_w*(i), img.rows - cvRound(tmp_hist.at<float>(i)));
		cv::line(img, p0, p1, cor, 1, 8, 0);
	}
}

int ColorDistanceSquared(const cv::Mat &img, int x0, int y0, int x1, int y1) {
	int dr, dg, db;
	db = img.at<cv::Vec3b>(y0, x0)[0] - img.at<cv::Vec3b>(y1, x1)[0];
	dg = img.at<cv::Vec3b>(y0, x0)[1] - img.at<cv::Vec3b>(y1, x1)[1];
	dr = img.at<cv::Vec3b>(y0, x0)[2] - img.at<cv::Vec3b>(y1, x1)[2];
	return db*db + dg*dg + dr*dr;
}

bool Border(const cv::Mat &img, int x, int y, int color_distance_squared_threshold) {
	if(	ColorDistanceSquared(img, x, y, x - 1, y) <= color_distance_squared_threshold &&
		ColorDistanceSquared(img, x, y, x, y - 1) <= color_distance_squared_threshold &&
		ColorDistanceSquared(img, x, y, x + 1, y) <= color_distance_squared_threshold &&
		ColorDistanceSquared(img, x, y, x, y + 1) <= color_distance_squared_threshold) {
			return true;
		}
	return false;
}

cv::Mat BICMask(const cv::Mat &img, int color_distance_squared_threshold) {
	cv::Mat mask(img.rows, img.cols, CV_8UC1, cv::Scalar(255));

	int x_max = img.cols - 1, y_max = img.rows - 1;
	for(int j = 1; j < y_max; ++j) {
		for(int i = 1; i < x_max; ++i) {
			if(!Border(img, i, j, color_distance_squared_threshold)) {
				mask.at<unsigned char>(j, i) = 0;
			}
		}
	}
	return mask;
}
